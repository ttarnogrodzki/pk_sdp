#include <iostream>

class Bazowa
{
public:
    virtual void wypisz()
        {std::cout << "Bazowa!!!" << std::endl;};
};

class Potomna : public Bazowa    
{
public:
    void wypisz()
        {std::cout << "Potomna!!!" << std::endl;};
};


class Klasa
{
public:
    int liczba;
    Klasa& operator=(const Klasa& that)
    {
        this->liczba = that.liczba;
        return *this;
    }
};

int main()
{
    Bazowa* klasa = new Potomna;
    klasa->wypisz();
    delete klasa;
    return 0;    
}