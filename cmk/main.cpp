#include <iostream>

#include "func.hpp"
 
int main() 
{
   std::cout << "Hello, world!" << std::endl;
   std::cout << func_sum(21, 21) << std::endl;
   return 0;
}