#include "func.hpp"

int func_sum(const int& v1, const int& v2)
{
    return v1 + v2;
}