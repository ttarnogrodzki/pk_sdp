#include <iostream>
#include <fstream>

int read_file(std::string filaname)
{
    std::ifstream my_file(filaname);
    if (my_file.is_open())
    {
        std::string line;
        while(getline(my_file, line))
        {
            std::cout << line << std::endl;
        }
        my_file.close();        
    }
    else
    {
        std::cout << "Coś poszło nie tak z odczytywaniem pliku!" << std::endl;
        return 1;
    }
    return 0;        
}

int main()
{
    std::cout << "Piki:" << std::endl << std::endl;;
    std::ofstream my_file;
    std::string file_name{"plik"};

    my_file.open(file_name);
    my_file << "jakiś tekst" << "i następny tekst" << std::endl;
    my_file << "a tutaj następna linia"<< std::endl;
    my_file << "i jeszcze jedna!!!"<< std::endl;    
    my_file.close();    
    read_file(file_name);

    std::cout << std::endl << "Plik, w trybie ios::app" << std::endl << std::endl;
    my_file.open(file_name, std::ios::app);
    my_file << "dodajemy nową linię!" << std::endl;
    my_file.close();    
    read_file(file_name);

    std::cout << std::endl << "Plik, standardowo w trybie ios::trunc" << std::endl << std::endl;
    my_file.open(file_name, std::ios::trunc);
    my_file << "dodajemy nową linię!" << std::endl;
    my_file.close();    
    read_file(file_name);

    return 0;
}