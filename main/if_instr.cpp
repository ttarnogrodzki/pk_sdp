#include <iostream>

int main()
{
    int i = 5, k = 12;
    if(i < 5)
    {
        std::cout << "i < 5! ";
    }        
    else
    {
        std::cout << "i >= 5! ";
    }

    if(k > 8)
    {
        std::cout << "k > 8! ";
    }        
    else
    {
        std::cout << "k <= 8! ";
    }

    return 0;
}


 