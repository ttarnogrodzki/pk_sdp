#include <iostream>

int main()
{
    std::cout   
        << "                   int : " << sizeof (int) << std::endl
        << "          unsigned int : " << sizeof (unsigned) << std::endl
        << "             short int : " << sizeof (short) << std::endl
        << "    unsigned short int : " << sizeof (unsigned short) << std::endl
        << "                  char : " << sizeof (char) << std::endl
        << "         unsigned char : " << sizeof (unsigned char) << std::endl
        << "              long int : " << sizeof (long) << std::endl
        << "     unsigned long int : " << sizeof (unsigned long) << std::endl
        << "         long long int : " << sizeof (long long) << std::endl
        << "unsigned long long int : " << sizeof (unsigned long long) << std::endl
        << "                 float : " << sizeof (float) << std::endl
        << "                double : " << sizeof (double) << std::endl
        << "           long double : " << sizeof (long double) << std::endl
        << "                  bool : " << sizeof (bool) << std::endl;

    return 0;
} 