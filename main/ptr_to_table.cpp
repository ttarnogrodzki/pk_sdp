#include <iostream>
 
int main () 
{
   double balance[5] = {1000.0, 2.0, 3.4, 17.0, 50.0};
   double *p;

   p = balance;

   std::cout << "Array values using pointer " << std::endl;

   for ( int i = 0; i < 5; i++ ) 
   {
      std::cout << "*(p + " << i << ") : ";
      //std::cout << *(p + i) << std::endl;
      std::cout << *p << std::endl;
      ++p;
   }
   std::cout << "Array values using balance as address " << std::endl;

   for ( int i = 0; i < 5; i++ ) 
   {
      std::cout << "*(balance + " << i << ") : ";
      std::cout << *(balance + i) << std::endl;
   }

   return 0;
}