#include <iostream>

int main()
{
    int t[10];
    for (std::size_t i = 0; i < 10; ++i)
        t[i] = i * 123;
    for (std::size_t i = 0; i < 10; ++i)        
        std::cout << "t[" << i << "]: " << t[i] << std::endl;
    
    return 0;
}