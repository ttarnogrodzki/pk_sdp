#include <iostream>

int main()
{
    int t[10][10];
    for (std::size_t i = 0; i < 10; ++i)
        for (std::size_t j = 0; j < 10; ++j)
        t[i][j] = (i+1) * (j+1);
    for (std::size_t i = 0; i < 10; ++i)        
    {
        for (std::size_t j = 0; j < 10; ++j)
            std::cout << t[i] [j] << "\t";
        std::cout <<std::endl;
    }
    return 0;
}