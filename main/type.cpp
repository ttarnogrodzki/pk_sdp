#include <iostream>

int main()
{
    std::cout   
        << "                   int : " << typeid (int).name() << std::endl
        << "          unsigned int : " << typeid (unsigned).name() << std::endl
        << "             short int : " << typeid (short).name() << std::endl
        << "    unsigned short int : " << typeid (unsigned short).name() << std::endl
        << "                  char : " << typeid (char).name() << std::endl
        << "         unsigned char : " << typeid (unsigned char).name() << std::endl
        << "              long int : " << typeid (long).name() << std::endl
        << "     unsigned long int : " << typeid (unsigned long).name() << std::endl
        << "         long long int : " << typeid (long long).name() << std::endl
        << "unsigned long long int : " << typeid (unsigned long long).name() << std::endl
        << "                 float : " << typeid (float).name() << std::endl
        << "                double : " << typeid (double).name() << std::endl
        << "           long double : " << typeid (long double).name() << std::endl
        << "                  bool : " << typeid (bool).name() << std::endl;

    return 0;
} 